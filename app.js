require('dotenv').config();

const Koa = require('koa');
const setupRoutes = require('./lib/setup-routes');
const connectToDb = require('./lib/connect-to-db');
const middleware = require('./lib/middleware');
const cors = require('@koa/cors');

connectToDb();

const PORT = process.env.PORT || 8080;

const app = new Koa();

app.on('error', (err, ctx) => {
  if (err.status === 500) {
    console.error(`Server Error - ${err.stack}`);
  }
});

app.use(async (ctx, next) => {
  console.log(`Request for ${ctx.method} ${ctx.path} received at ${Date.now()}`);
  await next();
});

app.use(cors());
app.use(middleware.errorHandler());
app.use(setupRoutes());

const server = app.listen(PORT, () => {
  console.log(`Listening on ${PORT}`);
});

module.exports = server;
